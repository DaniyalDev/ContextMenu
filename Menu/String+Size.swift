//
//  String+Size.swift
//  Menu
//
//  Created by Даниял Деветов on 17.12.2021.
//

import UIKit

extension String {
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        
        let fontAttributes = [NSAttributedString.Key.font: font]
        return (self as NSString).size(withAttributes: fontAttributes)
    }
}
