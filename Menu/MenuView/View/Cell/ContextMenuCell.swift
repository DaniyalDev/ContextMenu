//
//  ContextMenuCell.swift
//  Menu
//
//  Created by Даниял Деветов on 13.12.2021.
//

import UIKit

final class ContextMenuCell: UITableViewCell {
    
    // MARK: - Locals
    
    private enum Locals {
        static let titleFont: UIFont = .systemFont(ofSize: 17)
        static let titleLeftOffset: CGFloat = 20
        static let imageInsets = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 16)
        static let imageSize = CGSize(width: 24, height: 24)
        static let maximumCellSize = UIScreen.main.bounds.width - 16
    }
    
    
    // MARK: - Properties
    
    private let itemImageView = UIImageView()
    private let titleLabel = UILabel()
    
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        drawSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Overriding methods
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        // TODO: - Добавить цвет в основном проекте
        backgroundColor = highlighted ? .lightGray.withAlphaComponent(0.1) : .white
    }
    
    
    // MARK: - Drawing
    
    private func drawSelf() {
        
        titleLabel.font = Locals.titleFont
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(itemImageView)
        
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Locals.titleLeftOffset),
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            itemImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Locals.imageInsets.top),
            itemImageView.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: Locals.imageInsets.left),
            itemImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Locals.imageInsets.right),
            itemImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Locals.imageInsets.bottom),
            itemImageView.heightAnchor.constraint(equalToConstant: Locals.imageSize.height),
            itemImageView.widthAnchor.constraint(equalToConstant: Locals.imageSize.width)
        ])
    }
    
    
    // MARK: - Public methods
    
    func setup(with menuItem: ContextMenuItem) {
        
        itemImageView.image = menuItem.image
        titleLabel.text = menuItem.title
        itemImageView.tintColor = menuItem.colorStyle.imageColor
        titleLabel.textColor = menuItem.colorStyle.textColor
    }
    
    static func calculateCellWidth(with menuItem: ContextMenuItem) -> CGFloat {
        let width = Locals.titleLeftOffset +
                    menuItem.title.sizeOfString(usingFont: Locals.titleFont).width +
                    Locals.imageInsets.left +
                    Locals.imageSize.width +
                    Locals.imageInsets.right
        
        return min(width, Locals.maximumCellSize)
    }
}
