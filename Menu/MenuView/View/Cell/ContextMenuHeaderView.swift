//
//  MenuHeaderView.swift
//  Menu
//
//  Created by Даниял Деветов on 20.12.2021.
//

import UIKit

final class ContextMenuHeaderView: UITableViewHeaderFooterView {
    
    // MARK: - Init
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        drawSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Drawing
    
    private func drawSelf() {
        backgroundColor = .clear
        backgroundConfiguration = UIBackgroundConfiguration.clear()
        
        
        let backButton = UIButton()
        backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(backButton)
        
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: topAnchor, constant: 12),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            backButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12)
        ])
    }
}
