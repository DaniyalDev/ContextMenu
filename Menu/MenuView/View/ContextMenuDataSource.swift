//
//  ContextMenuDataSource.swift
//  Menu
//
//  Created by Даниял Деветов on 20.12.2021.
//

import UIKit

protocol ContextMenuDataSourceInput {
    var tableViewHeight: CGFloat { get }
    
    func setup(tableView: UITableView)
    func update(with items: [ContextMenuItem], type: ContextMenuDataSource.MenuType)
}

final class ContextMenuDataSource: NSObject, ContextMenuDataSourceInput {
    
    // MARK: - Nested models
    
    private enum Locals {
        static let tableViewContentInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        static let cellHeight: CGFloat = 40
        static let headerIdentifier = "ContextMenuHeaderView"
        static let cellIdentifier = "ContextMenuCell"
    }
    
    enum MenuType {
        case main
        case subMenu
    }
    
    
    // MARK: - Properties
    
    var tableViewHeight: CGFloat {
        var cellsHeight = Locals.tableViewContentInsets.top + Locals.tableViewContentInsets.bottom + Locals.cellHeight * CGFloat(items.count)
        
        if case .subMenu = menuType {
            cellsHeight += Locals.cellHeight
        }
        
        return cellsHeight
    }
    
    private var tableView: UITableView?
    private var items: [ContextMenuItem] = []
    private var menuType: MenuType = .main
    
    
    // MARK: - MenuDataSourceInput
    
    func setup(tableView: UITableView) {
        self.tableView = tableView
        
        tableView.register(ContextMenuCell.self, forCellReuseIdentifier: Locals.cellIdentifier)
        tableView.register(ContextMenuHeaderView.self, forHeaderFooterViewReuseIdentifier: Locals.headerIdentifier)
        tableView.contentInset = Locals.tableViewContentInsets
        tableView.backgroundColor = .white
        tableView.layer.cornerRadius = 12
        tableView.isScrollEnabled = false
        tableView.separatorInset = .zero
        tableView.allowsSelection = false
        // TODO: - Добавить цвет в основном проекте
        tableView.separatorColor = .black.withAlphaComponent(0.1)
        tableView.tableFooterView = UIView()
        tableView.sectionHeaderTopPadding = 0
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func update(with items: [ContextMenuItem], type: MenuType) {
        self.items = items
        self.menuType = type
        tableView?.reloadSections([0], with: type == .subMenu ? .left : .right)
    }
}


// MARK: - UITableViewDataSource, UITableViewDelegate
extension ContextMenuDataSource: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Locals.cellIdentifier, for: indexPath) as? ContextMenuCell else {
            return UITableViewCell()
        }
        
        cell.setup(with: items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard case .subMenu = menuType else {
            return nil
        }

        return tableView.dequeueReusableHeaderFooterView(withIdentifier: Locals.headerIdentifier)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Locals.cellHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if case .subMenu = menuType {
            return Locals.cellHeight
        } else {
            return 0
        }
    }
}
