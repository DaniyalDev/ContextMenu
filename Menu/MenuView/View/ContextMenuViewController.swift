//
//  ContextMenuViewController.swift
//  Menu
//
//  Created by Даниял Деветов on 13.12.2021.
//

import UIKit

final class ContextMenuViewController: UIViewController {
    
    // MARK: - Locals
    
    private enum Locals {
        static let tableViewContentInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        static let cellHeight: CGFloat = 40
        static let tableViewVerticalOffset: CGFloat = 20
        static let tableViewHorizontalOffset: CGFloat = 8
        static let minimumTableViewWidth: CGFloat = 204
    }
    
    
    // MARK: - Properties
    
    var snapshotView: UIView?
    var dataSource: ContextMenuDataSourceInput?
    
    private var menuItems: [ContextMenuItem]
    
    private let tableView = UITableView()
    private var lastHighlightedCell: UITableViewCell?
    private var isAnimated = false
    
    private var tableViewHeight: CGFloat {
        dataSource?.tableViewHeight ?? 0
    }
    
    private lazy var tableViewWidth: CGFloat = {
        let maximumCellWidth = menuItems.map {
            ContextMenuCell.calculateCellWidth(with: $0)
        }.sorted { $0 > $1 }.first ?? 0
        
        return max(maximumCellWidth, Locals.minimumTableViewWidth)
    }()
    
    
    // MARK: - Init
    
    init(menuItems: [ContextMenuItem]) {
        self.menuItems = menuItems
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Lifecycle
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard !isAnimated else {
            return
        }
        
        drawSelf()
        isAnimated = true
    }
    
    
    // MARK: - Drawing
    
    private func drawSelf() {
        
        guard let snapshotView = snapshotView else {
            return
        }
        
        view.backgroundColor = .clear
        
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.backgroundColor = .clear
        blurView.frame = view.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnOutside)))
        
        dataSource?.setup(tableView: tableView)
        dataSource?.update(with: menuItems, type: .main)
        
        let tableLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(tableLongPressAction(_:)))
        tableLongPressGesture.minimumPressDuration = 0
        tableView.addGestureRecognizer(tableLongPressGesture)
        
        view.addSubview(blurView)
        view.addSubview(tableView)
        view.addSubview(snapshotView)
        
        let calculatedYPositions = calculateYPositions()
        let calculatedXPositions = calculateXPositions()
        
        tableView.frame = CGRect(x: calculatedXPositions.tableViewXPosition,
                                 y: calculatedYPositions.tableViewYPosition,
                                 width: tableViewWidth,
                                 height: tableViewHeight)
        
        tableView.setAnchorPoint(.zero)
        tableView.transform = tableView.transform.scaledBy(x: 0, y: 0)

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {

            snapshotView.frame.origin.y -= calculatedYPositions.snapshotYDelta
            snapshotView.frame.origin.x -= calculatedXPositions.snapshotXDelta
            self.tableView.transform = .identity
        }, completion: nil)
        
        tableView.reloadData()
    }
    
    
    // MARK: - Actions
    
    @objc private func tableLongPressAction(_ gesture: UILongPressGestureRecognizer) {
        
        switch gesture.state {
            
        case .began, .changed:
            gestureMoves(on: gesture.location(in: view))
            
        case .ended:
            gestureEnded(on: gesture.location(in: view))
            
        default:
            break
        }
    }
    
    @objc private func didTapOnOutside() {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Public methods
    
    func gestureMoves(on point: CGPoint) {
        
        guard let cellWithIndex = cell(in: point) else {
                  
            lastHighlightedCell?.isHighlighted = false
            lastHighlightedCell = nil
            return
        }
        
        if !(cellWithIndex.cell === lastHighlightedCell) {
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            lastHighlightedCell?.isHighlighted = false
            cellWithIndex.cell.isHighlighted = true
            lastHighlightedCell = cellWithIndex.cell
        }
    }
    
    func gestureEnded(on point: CGPoint) {
        
        if gestureStopsOnHeader(in: point) {
            dataSource?.update(with: menuItems, type: .main)
            updateTableViewHeightAnimated()
            
        } else if let cellOnPoint = cell(in: point) {
            handleItemAction(menuItems[cellOnPoint.indexPath.row])
            
        } else {
            lastHighlightedCell?.isHighlighted = false
            lastHighlightedCell = nil
            return
        }
    }
    
    
    // MARK: - Private methods
    
    private func calculateYPositions() -> (tableViewYPosition: CGFloat, snapshotYDelta: CGFloat) {
        
        guard let snapshotView = snapshotView else {
            return (0, 0)
        }
        
        let potentialTableViewYPosition = snapshotView.frame.origin.y + snapshotView.frame.height + Locals.tableViewVerticalOffset
        let viewHeight = view.bounds.height - Locals.tableViewVerticalOffset - view.safeAreaInsets.bottom
        
        if potentialTableViewYPosition + tableViewHeight < viewHeight {
            return (potentialTableViewYPosition, 0)
            
        } else {
            let delta = potentialTableViewYPosition + tableViewHeight - viewHeight
            let tableYPosition = potentialTableViewYPosition - delta
            return (tableYPosition, delta)
        }
    }
    
    private func calculateXPositions() -> (tableViewXPosition: CGFloat, snapshotXDelta: CGFloat) {
        
        let potentialTableViewXPosition = (snapshotView?.frame.origin.x ?? 0)
        let viewWidth = view.bounds.width - Locals.tableViewHorizontalOffset - view.safeAreaInsets.right
        
        if potentialTableViewXPosition + tableViewWidth < viewWidth {
            return (potentialTableViewXPosition, 0)
            
        } else {
            let delta = potentialTableViewXPosition + tableViewWidth - viewWidth
            let tableXPosition = potentialTableViewXPosition - delta
            return (tableXPosition, delta)
        }
    }
    
    private func cell(in point: CGPoint) -> (cell: UITableViewCell, indexPath: IndexPath)? {
        
        let pointInTable = view.convert(point, to: tableView)
        
        guard let rowIndex = tableView.indexPathForRow(at: pointInTable),
              let cellOnPoint = tableView.cellForRow(at: rowIndex) else {
                  
            return nil
        }
        
        return (cellOnPoint, rowIndex)
    }
    
    private func gestureStopsOnHeader(in point: CGPoint) -> Bool {
        let pointInTable = view.convert(point, to: tableView)
        return tableView.headerView(forSection: 0)?.frame.contains(pointInTable) == true
    }
    
    private func handleItemAction(_ item: ContextMenuItem) {
        
        switch item.type {
            
        case .simple(let action):
            action()
            dismiss(animated: true, completion: nil)
            
        case .group(let items):
            dataSource?.update(with: items, type: .subMenu)
            updateTableViewHeightAnimated()
        }
    }
    
    private func updateTableViewHeightAnimated() {
        UIView.animate(withDuration: 0.3) {
            self.tableView.frame.size.height = self.tableViewHeight
        }
    }
}
