//
//  ContextMenuItem.swift
//  Menu
//
//  Created by Даниял Деветов on 13.12.2021.
//

import UIKit

@resultBuilder struct ContextMenuBuilder {
    
    static func buildBlock(_ components: ContextMenuItemConvertible...) -> ContextMenuViewController {
        let items = components.map { $0.toMenuItem() }
        let dataSource = ContextMenuDataSource()
        let menuViewController = ContextMenuViewController(menuItems: items)
        
        menuViewController.dataSource = dataSource
        menuViewController.modalPresentationStyle = .overFullScreen
        menuViewController.modalTransitionStyle = .crossDissolve
        
        return menuViewController
    }
}

@resultBuilder struct ContextMenuItemBuilder {
    
    static func buildBlock(_ components: ContextMenuItemConvertible...) -> [ContextMenuItem] {
        return components.map { $0.toMenuItem() }
    }
}

protocol ContextMenuItemConvertible {
    func toMenuItem() -> ContextMenuItem
}

struct ContextMenuItem: ContextMenuItemConvertible {
    
    let title: String
    let image: UIImage?
    let colorStyle: ColorStyle
    let type: ItemType
    
    func toMenuItem() -> ContextMenuItem {
        self
    }
    
    enum ItemType {
        case simple(() -> Void)
        case group(items: [ContextMenuItem])
    }
    
    enum ColorStyle {
        case simple
        case red
        
        
        // TODO: - Добавить цвет в основном проекте
        var textColor: UIColor {
            
            switch self {
            
            case .simple:
                return .black
                
            case .red:
                return .red
            }
        }
        
        var imageColor: UIColor {
            
            switch self {
            
            case .simple:
                return .blue
                
            case .red:
                return .red
            }
        }
    }
}

struct GroupContextMenuItem: ContextMenuItemConvertible {
    let title: String
    let image: UIImage?
    let colorStyle: ContextMenuItem.ColorStyle
    @ContextMenuItemBuilder let items: () -> [ContextMenuItem]
    
    func toMenuItem() -> ContextMenuItem {
        ContextMenuItem(title: title, image: image, colorStyle: colorStyle, type: .group(items: items()))
    }
}
