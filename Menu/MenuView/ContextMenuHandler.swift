//
//  ContextMenuHandler.swift
//  Menu
//
//  Created by Даниял Деветов on 20.12.2021.
//

import UIKit

final class ContextMenuHandler {
    
    // MARK: - Properties
    
    private weak var contextMenuViewController: ContextMenuViewController?
    
    
    // MARK: - Public methods
    
    func addContextMenuGesture(for view: UIView) {
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction(_:)))
        view.addGestureRecognizer(gesture)
    }
    
    @objc func longPressAction(_ gesture: UILongPressGestureRecognizer) {
        
        guard let view = gesture.view else {
            return
        }

        switch gesture.state {

        case .began:
            
            guard let snapShot = view.snapshotView(afterScreenUpdates: true) else {
                return
            }
            
            UIImpactFeedbackGenerator(style: .heavy).impactOccurred()

            CATransaction.begin()
            CATransaction.setCompletionBlock({
                
                guard let menuViewController = (view as? ContextMenuPresentable)?.menuBuilder else {
                    return
                }
                
                self.contextMenuViewController = menuViewController
                menuViewController.snapshotView = snapShot
                UIApplication.shared.keyWindow?.rootViewController?.present(menuViewController, animated: true, completion: nil)
            })

            let basicAnimation = CABasicAnimation(keyPath: "transform.scale")
            basicAnimation.fromValue = 1
            basicAnimation.toValue = 0.9
            basicAnimation.autoreverses = true
            basicAnimation.duration = 0.15
            gesture.view?.layer.add(basicAnimation, forKey: "transfromScale")
            CATransaction.commit()
            snapShot.frame = gesture.view?.frame ?? .zero

        case .changed:
            contextMenuViewController?.gestureMoves(on: gesture.location(in: contextMenuViewController?.view))
            
        case .ended:
            contextMenuViewController?.gestureEnded(on: gesture.location(in: contextMenuViewController?.view))
            
        default:
            break
        }
    }
}
