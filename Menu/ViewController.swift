//
//  ViewController.swift
//  Menu
//
//  Created by Даниял Деветов on 13.12.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .green.withAlphaComponent(0.5)
        
        let simpleView = MenuPresentableView(frame: CGRect(x: 100, y: 250, width: 100, height: 100))
        simpleView.backgroundColor = .red
        simpleView.addContextMenuGesture()
        view.addSubview(simpleView)
    }


}

