//
//  MenuPresentableView.swift
//  Menu
//
//  Created by Даниял Деветов on 17.12.2021.
//

import UIKit

final class MenuPresentableView: UIView, ContextMenuPresentable {
    let menuHandler = ContextMenuHandler()
    
    @ContextMenuBuilder var menuBuilder: ContextMenuViewController {
        ContextMenuItem(title: "Title 1", image: nil, colorStyle: .simple, type: .simple({ }))
        ContextMenuItem(title: "Title 2", image: nil, colorStyle: .simple, type: .simple({ }))
        GroupContextMenuItem(title: "Group", image: nil, colorStyle: .simple) {
            ContextMenuItem(title: "Title 1", image: nil, colorStyle: .simple, type: .simple({ }))
            ContextMenuItem(title: "Title 2", image: nil, colorStyle: .simple, type: .simple({ }))
            ContextMenuItem(title: "Title 3", image: nil, colorStyle: .simple, type: .simple({ }))
        }
        ContextMenuItem(title: "Title 3", image: nil, colorStyle: .simple, type: .simple({ }))
        ContextMenuItem(title: "Title 4", image: nil, colorStyle: .simple, type: .simple({ }))
        ContextMenuItem(title: "Title 5", image: nil, colorStyle: .simple, type: .simple({ }))
    }
}
