//
//  ContextMenuPresentable.swift
//  Menu
//
//  Created by Даниял Деветов on 13.12.2021.
//

import UIKit

protocol ContextMenuPresentable {
    @ContextMenuBuilder var menuBuilder: ContextMenuViewController { get }
    var menuHandler: ContextMenuHandler { get }
    func addContextMenuGesture()
}

extension ContextMenuPresentable where Self: UIView {
    
    func addContextMenuGesture() {
        menuHandler.addContextMenuGesture(for: self)
    }
}
